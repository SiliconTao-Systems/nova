resource "aws_subnet" "dev_subnet" {
  availability_zone       = "${var.site.region}b"
	cidr_block							= "172.20.32.0/20"
  map_public_ip_on_launch = false
	vpc_id									= aws_vpc.production.id
  tags              = {
    Name = "Production"
  }
}

// VPN needs to route to this subnet to standup the EC2 web server.
// The VPN can be associated to mutiple subnets but they must be in different AZs
// https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/cvpn-working-target.html

resource "aws_subnet" "elb_subnet" {
  availability_zone       = "${var.site.region}b"
	cidr_block							= "172.20.64.0/20"
  map_public_ip_on_launch = false
	vpc_id									= aws_vpc.production.id
  tags              = {
    Name = "Load Balancer"
  }
}

resource "aws_subnet" "prod_subnet" {
  availability_zone       = "${var.site.region}a"
	cidr_block							= "172.20.0.0/20"
  map_public_ip_on_launch = false
	vpc_id									= aws_vpc.production.id
  tags              = {
    Name = "Production"
  }
}

resource "aws_subnet" "public_subnet" {
  availability_zone       = "${var.site.region}b"
	cidr_block							= "172.20.16.0/20"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.production.id
  tags              = {
    Name = "Public"
  }
}
