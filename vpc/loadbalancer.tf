locals {
  webhosts = [for I in var.instances : I.hostname if contains(I.security_groups, "web-server")]
  # websetinstances = toset([for I in var.aws_instances : I.id if contains(I.security_groups, "web-server") ])
	webinstances = toset([for I in var.aws_instances : I.id if contains(local.webhosts, I.tags["Name"]) ])
}

resource "aws_elb" "web_server_ingress_elb" {
  name               = "web-server-ingress-elb"
  count              = length(local.webinstances) > 0 ? 1 : 0
	subnets            = [ aws_subnet.elb_subnet.id ]
  security_groups    = [ "web-server" ]
  # access_logs {
  #   bucket        = "foo"
  #   bucket_prefix = "bar"
  #   interval      = 60
  # }

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  # listener {
  #   instance_port      = 443
  #   instance_protocol  = "http"
  #   lb_port            = 443
  #   lb_protocol        = "https"
  #   ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
  # }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

	# this did not assign any instances to the ELB
	# FIX THIS!!!
	instances                   = local.webinstances
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "web_server_ingress_elb"
  }
}

output "elb" {
	value = aws_elb.web_server_ingress_elb
}