# https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/what-is.html

resource "aws_ec2_client_vpn_endpoint" "vpn_endpoint" {
  description             = "Production VPN"
  server_certificate_arn  = var.certs.cert
  client_cidr_block       = "10.0.0.0/16"
  split_tunnel            = var.vpc[0].vpn[0].split_tunnel

  # This feature isn't available until Terraform AWS provider 4
  # client_login_banner_options {
  #   banner_text = "Welcome to ${var.site.domain} VPN"
  #   enabled     = true
  # }

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = var.certs.key
  }

  connection_log_options {
     enabled               = false
  #   cloudwatch_log_group  = aws_cloudwatch_log_group.lg.name
  #   cloudwatch_log_stream = aws_cloudwatch_log_stream.ls.name
  }
}

resource "aws_ec2_client_vpn_authorization_rule" "prod_auth_rule" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
  target_network_cidr    = aws_subnet.prod_subnet.cidr_block
  authorize_all_groups   = true
}

resource "aws_ec2_client_vpn_authorization_rule" "elb_auth_rule" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
  target_network_cidr    = aws_subnet.elb_subnet.cidr_block
  authorize_all_groups   = true
}

resource "aws_ec2_client_vpn_network_association" "prod_vpn_association" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
  subnet_id              = aws_subnet.prod_subnet.id
  security_groups        = [ var.sgs["vpn-server"] ]
}

resource "aws_ec2_client_vpn_network_association" "elb_vpn_association" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
  subnet_id              = aws_subnet.elb_subnet.id
  security_groups        = [ var.sgs["vpn-server"] ]
}

output "vpn_endpoint" {
	value = [ aws_ec2_client_vpn_endpoint.vpn_endpoint.id ]
}