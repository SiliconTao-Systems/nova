resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.production.id

  tags = {
    Name = "Internet Gateway"
  }
}

resource "aws_nat_gateway" "nat_gw" {
	allocation_id = aws_eip.public_ip.id
	subnet_id			= aws_subnet.public_subnet.id
}

resource "aws_vpn_gateway" "vpn_gw" {
  vpc_id = aws_vpc.production.id

  tags = {
    Name = "Production"
  }
}

resource "aws_route_table" "prod_ngw" {
  vpc_id    = aws_vpc.production.id
  route   {
    cidr_block      = "0.0.0.0/0"
    nat_gateway_id  = aws_nat_gateway.nat_gw.id
  }
}

resource "aws_route_table_association" "prod_route" {
  route_table_id    = aws_route_table.prod_ngw.id
  subnet_id         = aws_subnet.prod_subnet.id
}

resource "aws_route_table" "public_igw" {
  vpc_id    = aws_vpc.production.id
  route   {
    cidr_block      = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.internet_gw.id
  }
}

resource "aws_route_table_association" "public_route" {
  route_table_id    = aws_route_table.public_igw.id
  subnet_id         = aws_subnet.public_subnet.id
}

resource "aws_route_table_association" "elb_route" {
  # for setup, use ngw, then switch to igw to serve site to the public
  # route_table_id    = aws_route_table.public_igw.id
  route_table_id    = aws_route_table.prod_ngw.id
  subnet_id         = aws_subnet.elb_subnet.id
}