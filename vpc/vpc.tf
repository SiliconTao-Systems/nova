resource "aws_vpc" "production" {
  enable_dns_hostnames	= true
	cidr_block						= "172.20.0.0/16"
  tags = {
    Name = "Production VPC"
  }
}
