# VPC
## Default VPC vs. VPC
Every resource in AWS exists within a [VPC](## "Virtual Private Cloud"). A VPC is analogous to a [LAN](## "Local Area Network"). A VPC isolates resources from those in other VPCs and special networking resources are needed to make connections between them. See [VPC Peering Connection](https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.html)

If a VPC is not created, the **Default VPC** is used. Nova makes no attempt to alter the **Default VPC**, custom changes to VPC configurations are made in a new VPC that Terraform can control.


|   |   |
|---|---|
| [![Wordpress](../images/wordpress.png)](../images/wordpress.png) | If the environment only contains EC2 Instances with public IP address, the **Default VPC** is sufficient. The provided example can be found in [wordpress.json](../wordpress.json) and is a simple environment with few resources. The Default VPC is not the recommended for production and it is prone to loosing control of the public IP address if the EC2 Instance is rebuilt. |
| [![VPC with VPN](../images/VPC_with_VPN.png)](../images/VPC_with_VPN.png) | Adding the VPC configurations to the **nova.json** file creates a new VPC that places private resources in private subnets. This automatically add a [NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html) for [egress](## "Out going") from the private subnet, and an [Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html) for [ingress](## "in coming") from the public Internet. The [EIP](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-eips.html) is associated to the Internet Gateway this the EC2 Instance can be freely changed, destroyed and rebuilt with out loosing control of the public IP address. This option also creates a VPN to allow SSH access from the the administrators computer on the public Internet. The provided example can be found in [mediahouse.json](../mediahouse.json).|

# VPC
To enable VPC in the Nova project include the VPC element in the JSON file like so.
```json
	"vpc": [
		{
			"vpn": [
				{
					"split_tunnel": true
				}
			]
		}
	]
```

Using the custom VPC will enable a private VPN connection for administration access to the VPC. The option for **split_tunnel** changes the VPN so that public Internet access can be directed to travel via the VPN tunnel or not. Using **split_tunnel = true** will allow direct Internet access as normal, setting **split_tunnel = false** will cause all internet traffic to travel though the VPN.