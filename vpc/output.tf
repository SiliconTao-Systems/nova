output "vpc_resources" {
	value = {
		public_ip			= aws_eip.public_ip,
		dev_subnet		= aws_subnet.dev_subnet,
		elb_subnet		= aws_subnet.elb_subnet,
		prod_subnet		= aws_subnet.prod_subnet,
		public_subnet = aws_subnet.public_subnet,
		vpc_prod			= aws_vpc.production
	}
}