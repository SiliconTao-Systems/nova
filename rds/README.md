# RDS
[RDS](https://aws.amazon.com/rds/) is the AWS Relational Database Service, their cloud service for SQL Database.

An example of adding this resource can be found in [wordpress.json](../wordpress.json)

```json
   "rds": [{
      "db_name": "wordpress",
      "db_size": 10,
      "db_engine": "MySQL",
      "db_performance": "db.t2.micro",
      "db_username": "sample_user",
      "db_password": "1234passThed00r",
      "security_groups": [
         "sql-server"
      ]
   }]
```