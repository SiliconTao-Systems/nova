resource "aws_db_instance" "database" {
	allocated_storage				=	var.rds[count.index].db_size
	availability_zone       = length(var.vpc) > 0 ? var.AvailabilityZones[0].ZoneName : null
	count										= length(var.rds)
	//db_subnet_group_name		= "db_subnet"
	db_subnet_group_name 		= length(var.vpc) > 0 ? aws_db_subnet_group.db_subnet[0].id : null
	engine									= var.rds[count.index].db_engine
	instance_class					= var.rds[count.index].db_performance
	name										= var.rds[count.index].db_name
	password								= var.rds[count.index].db_password
	skip_final_snapshot			= true
	username								= var.rds[count.index].db_username
	vpc_security_group_ids	= [ for sg in var.rds[count.index].security_groups: var.sgs[sg] ]
}

resource "aws_db_subnet_group" "db_subnet" {
	name				= "db_subnet"
	count 			= length(var.vpc) > 0 ? 1 : 0
	subnet_ids 	= length(var.vpc) > 0 ? [
		var.vpc_resources.prod_subnet.id,
		var.vpc_resources.dev_subnet.id
	] : [
		var.default_vpc.default_subnet_a.id,
		var.default_vpc.default_subnet_b.id
	]
}

output "rds_endpoints" {
  # value = "${aws_db_instance.default.endpoint}"
	value = [ for rdep in aws_db_instance.database[*]:
		{
			name = rdep.name
			endpoint = rdep.endpoint
		}
	]
}
