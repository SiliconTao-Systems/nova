variable "AvailabilityZones" {
	description	= "Availability Zones for the selected region."
	default			= []
}

variable "default_vpc" {
	description = "The default VPC used if a VPC is not created."
	default 		= []
}

variable "rds" {
	description = "The nova.json rds"
}

variable "site" {
	description = "Nova.json values for the site"
}

variable "sgs" {
	type = map
}

variable "vpc" {
	description	= "The nova.json vpc"
	default			= []
}

variable "vpc_resources" {
	description = "AWS VPC resources"
	default			= []
}
