.PHONY: help

help: ## Show the recipes in this Makefile. Like so `make help`
	@echo "----------------------------------------"
	@echo "|            Makefile help             |"
	@echo "----------------------------------------"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

CMD=terraform
TRS=$($@)

.AUTO:
	# Comment out to stop automatic acceptence
	@echo "AUTO_ACCEPT is enabled in the Makefile by default for this project."
	$(eval export AUTO_ACCEPT=-auto-approve)

auto: .AUTO ## Set automatically accept changes without prompting.


.FORCE:

silent:
	@:

DEBUG: ## Enable TF_LOG to some more information
	$(eval export TF_LOG=DEBUG)

.TARGET: ## REGIONs that the required TARGET is set
	$(if $(value TARGET),, $(if $(value ADDRESS), $(eval export TARGET=$(ADDRESS)), @printf "\033[31m%s\033[0m " Error; echo Please set the value of TARGET; make help; exit 1))

firstinit: ## Automatically run init if it has not yet been done.
	@[ -d .terraform ] || $(CMD) init

init: .FORCE ## Init to initialize Terraform for this project. Also used when Terraform is upgraded.
	$(CMD) init

creadentials: ## Populates the credentials file from the AWS files.
	@jq -Rn '{ "region": "'$(shell aws configure get region)'", "access_key": "'$(shell aws configure get default.aws_access_key_id)'", "secret_key": "'$(shell aws configure get default.aws_secret_access_key)'" }' > credentials.json

getAZ: nova
	echo getAZ
	aws ec2 describe-availability-zones --region $(shell cat nova.json | jq --raw-output '.site.region') > availability-zones.json

validate: .FORCE firstinit creadentials getAZ ## Check the syntax of the plans
	$(CMD) validate
	@find . -type f -name "*.sh" -exec bash -n "{}" \;

plan: .FORCE nova validate  ## Generate the deployment plan. Must be proceeded by profile 'make US plan'
ifneq ($(wildcard key_cert.json),)
	$(eval export CERTS_VAR="-var-file=key_cert.json")
endif
	$(CMD) plan $(if $(value TARGET), "-target=$(TARGET)", ) $(AUTO_ACCEPT) $(CERTS_VAR) -var-file=./availability-zones.json -var-file=./credentials.json -var-file=nova.json

.SSHKEY: # Generate SSH key for the instance
	[ -f ./AWS_ALX_ssh_key ] || ssh-keygen -t rsa -b 2048 -f ./AWS_ALX_ssh_key -q -N ""
	$(if $(value SSH_AGENT_PID), $(shell ssh-add ./AWS_ALX_ssh_key), )

getTwoStage:
	@echo "getTwoStage"
ifeq ($(shell terraform output VPC_resources | grep -q public_subnet && echo 0 || echo 1),1)
	@echo "VPC_resources does not exist"
	$(eval export TF_VAR_twostage=$(shell cat nova.json | jq '.vpc | length'))
	make vpnCert
else
	@echo "VPC_resources does exist and will be used"
	$(eval export TF_VAR_twostage=0)
	make vpnConnect
endif

vpnConnect:
	@echo "vpnConnect"
ifeq ($(shell cat nova.json | jq '.vpc | length'),1)
	@echo "VPC_resources is ready to connect"
	@bash ./toolbox/connectVPN.sh &
else
	@echo "VPC_resources will not be used"
endif

# -var="payload_dir=$(payload_dir)"
apply: .SSHKEY .FORCE nova getAZ validate #.AUTO validate ## Deploy the plans
	@echo apply
	make getTwoStage applycmd

applycmd:
	@echo "applycmd"
ifneq ($(wildcard key_cert.json),)
	$(eval export CERTS_VAR="-var-file=key_cert.json")
endif
	echo "CERTS_VAR = '${CERTS_VAR}'"
	echo "TF_VAR_twostage= '${TF_VAR_twostage}'"
	time $(CMD) apply $(if $(value TARGET), "-target=$(TARGET)", ) $(AUTO_ACCEPT) $(CERTS_VAR) -var-file=availability-zones.json -var-file=./credentials.json -var-file=nova.json 2>&1 | tee apply.log
	$(CMD) output
	make secondStage
	@if ./toolbox/get_public_IP.sh | grep -q [1-9]; then \
		bash ./toolbox/connectSamba.sh; \
		./toolbox/get_SSH_IP.sh; \
		./toolbox/openBrowser.sh; \
	fi
	@if cat nova.json|jq '.site' | grep -q "block_public.*true"; then \
		echo -e "\n---------------------------------\n"; \
		echo "Public access is blocked"; \
		echo "To remove public block run 'make unblock'"; \
		echo -e "\n---------------------------------\n"; \
	fi

unblock: ## To allow public access to HTTP port
	aws ec2 authorize-security-group-ingress --group-id $(shell terraform state show module.security.aws_security_group.web-server|grep "id.*=.*sg-"|tr -d \"|awk '{print $$3}') --cidr "0.0.0.0/0" --protocol tcp --port 80 ||:
	aws ec2 authorize-security-group-ingress --group-id $(shell terraform state show module.security.aws_security_group.web-server|grep "id.*=.*sg-"|tr -d \"|awk '{print $$3}') --cidr "0.0.0.0/0" --protocol tcp --port 443 ||:

# https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/client-authentication.html
secondStage:
ifeq ($(TF_VAR_twostage),1)
	@echo secondStage
	$(eval export TF_VAR_twostage=0)
	$(eval export TF_LOG=DEBUG)
	$(eval export TF_VAR_vpn_endpoint=$(shell terraform output vpn_endpoint|grep cvpn|cut -d\" -f2))
	aws ec2 export-client-vpn-client-configuration --client-vpn-endpoint-id $(TF_VAR_vpn_endpoint) --output text > config.ovpn
	bash  ./toolbox/connectVPN.sh &
	time $(CMD) apply $(if $(value TARGET), "-target=$(TARGET)", ) $(AUTO_ACCEPT) $(CERTS_VAR) -var-file=availability-zones.json -var-file=./credentials.json -var-file=nova.json 2>&1 | tee apply.log
endif

vpnCert:
ifeq ($(wildcard key_cert.json),)
ifeq ($(TF_VAR_twostage),1)
	@bash ./toolbox/cert.sh
endif
endif

autoapply: auto apply #.AUTO validate ## Deploy the plans

list: .FORCE ## List all resources currently in 'state'
	$(CMD) state list

show: .TARGET .FORCE ## Show the 'state' of a given resource. Requires TARGET=<MODULE> (AKA Address)
	$(CMD) state show $(TARGET)

taint: .TARGET .FORCE ## Force replace on next apply. Requires TARGET=<MODULE> (AKA Address)
	$(CMD) taint $(TARGET)

destroy: .FORCE nova #.AUTO ## To remove everything that Terraform built with apply.
	sudo killall openvpn ||:
ifneq ($(wildcard key_cert.json),)
	$(eval export CERTS_VAR="-var-file=key_cert.json")
endif
	$(CMD) destroy $(if $(value TARGET), "-target=$(TARGET)", ) $(AUTO_ACCEPT) $(CERTS_VAR) -var-file=availability-zones.json -var-file=./credentials.json -var-file=nova.json 2>&1 | tee destroy.log
	rm -rf ket_cert* AWS_ALX_ssh_key* *ovpn

# https://github.com/hashicorp/terraform-provider-aws/issues/4982
destroyall: .FORCE .AUTO nova ## Destroy instance before security groups, workaround unresolved Terraform bug.
# $(CMD) destroy -target=module.instance.aws_instance.alx $(AUTO_ACCEPT) -var-file=./credentials.json 2>&1 | tee destroy.log
	$(CMD) destroy $(AUTO_ACCEPT) -var-file=./credentials.json -var-file=nova.json 2>&1 | tee -a destroy.log
	aws ec2 describe-volumes | jq '.[][]|select(.State == "available")|{VolumeId,State,CreateTime}'
	aws ec2 describe-volumes | jq --raw-output '.[][]|select(.State == "available")|.VolumeId' | xargs -L1 -p aws ec2 delete-volume --no-prompt --volume-id

debug: ## Turn on debug logging before command.
   $(eval export TF_LOG=DEBUG)
   $(eval export TF_LOG_PATH=debug.log)

refresh: nova
	echo "Use nova file nova.json"
	$(CMD) refresh -var-file=./credentials.json -var-file=nova.json -var-file=availability-zones.json

nova: ## Read NOVA JSON
	echo nova
	$(if $(value URL), cp $(URL) nova.json, cp mediahouse.json nova.json)
	echo "URL=$(URL)"
ifeq ($(findstring git://,$(URL)),git://)
	echo "Download from Git repo $(URL)"
	# make a temp dir
	$(eval NDIR=$(mktemp -d /tmp/NOVA_XXXXXXXXX))
	# download using git
	git checkout $URL -- $(NDIR)
	cp $(basename ${URL//.gi/})/nova.json ./ || exit 160
else
	echo "Try to use local";
	# if not a nova.json file, exit
	$(eval export TF_VAR_nova_file=$(URL))
endif
	echo ${TF_VAR_nova_file} > last.nova
	echo "Use nova file ${TF_VAR_nova_file}"
	$(eval export TF_VAR_home_dir=${HOME})
