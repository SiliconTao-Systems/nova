# S3
Amazon Simple Storage Service or [S3](https://aws.amazon.com/s3/) is a very large, slow, but affordable way to store files.

Add an S3 to your project by insluding the following in your **nova.json** file.
```json
	"s3": [{
		"music": {
			"comment": "For mStream music"
		},
		"configs": {
			"comment": "Shared configuration files"
		}
	}],
```
