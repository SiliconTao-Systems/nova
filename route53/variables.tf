variable "instances" {
	default = "The nova.json instances"
}

variable "host_ips" {
	# type 				= map
	description = "FQDNs & Public facing IP addresses from Instances"
	default			= []
}

variable "site" {
	description = "Nova.json values for the site"
}