resource "aws_route53_zone" "private_zone" {
	name = var.site.domain
}

output "private_zone" {
	value = aws_route53_zone.private_zone.zone_id
}