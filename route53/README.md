# Route53
This is the internal DNS for resolving hostnames of instances to IP addresses.

DNS A records are automatically added by Nova for each EC2 instance.