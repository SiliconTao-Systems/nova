
resource "aws_route53_record" "private" {
	count						= length(var.host_ips)
	name 						= var.host_ips[count.index].hostname
	records 				= [ var.host_ips[count.index].private_ip ]
	ttl         		= 300
	type        		= "A"
	zone_id     		= aws_route53_zone.private_zone.zone_id
	allow_overwrite = true
}
