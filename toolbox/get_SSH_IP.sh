#!/bin/bash

terraform output -json > nova.output.json

echo "Available SSH connections"
cat nova.json | jq '.vpc | length' | grep -q 1 && {
	cat nova.output.json | jq --raw-output '.instances.value[].host_ips[]|"ssh alpine@\(.private_ip) (\(.hostname))"' | grep --color=never "[1-9]"
} || {
	cat nova.output.json | jq --raw-output '.instances.value[].host_ips[].public_ip' | grep --color=never "[1-9]"
}
