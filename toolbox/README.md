# Toolbox
These tools are used to assist with the setup of the environment.

* [connectSamba.sh](./connectSamba.sh) if an instance uses the [samba](../setup/alpine_samba.sh) script to setup Samba sharing, this script opens a connection to the service.

* [connectVPN.sh](./connectVPN.sh) if the custom [VPC](../vpc/README.md) is used, this script opens the VPN client connection from the administrators computer