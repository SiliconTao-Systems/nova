#!/bin/bash

exit 0
SMB_SERVER=$(cat nova.json | jq --raw-output '.instances | .[]|select(.setup_scripts[] | contains("samba")).hostname' 2>/dev/null)
[[ ${SMB_SERVER} == "" ]] && exit 0
echo "SMB_SERVER='$SMB_SERVER'"

# Clean out the old mount point
sudo umount AWS_SAMBA_* 2>/dev/null
rmdir AWS_SAMBA_* 2>/dev/null

SMB_IP=$(terraform output -json | jq --raw-output '.instances.value[].host_ips[] | select(.hostname == "'${SMB_SERVER}'").private_ip')
echo "SMB_IP='${SMB_IP}'"
[[ ${SMB_SERVER} == "" ]] && {
	echo "Could not find SMB server IP"
	exit 8
}
smbclient -N -L ${SMB_IP} || {
	echo "Could not query SMB server ${SMB_IP}"
	exit 14
}
TMPD=${PWD}/$(mktemp -d "AWS_SAMBA_${SMB_IP}_XXXXXXXXXXXXXXX")
echo "sudo mount -t cifs -o guest //${SMB_IP}/storage/ ${TMPD}"

sudo mount -t cifs -o guest,uid=$(id -u) //${SMB_IP}/storage ${TMPD}
xdg-open ${TMPD}
