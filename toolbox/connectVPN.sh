#!/bin/bash
DOMAIN=$(cat nova.json | jq --raw-output '.site.domain')
echo "DOMAIN=$DOMAIN"
sudo killall openvpn
gnome-terminal -- sh -c 'sudo openvpn --config config.ovpn --key key_cert/'${DOMAIN}'.key --cert key_cert/'${DOMAIN}'.crt; sleep 30'