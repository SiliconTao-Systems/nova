#!/bin/bash

terraform output -json > nova.output.json

# Only one of these will output an IP address
(
	cat nova.output.json | jq --raw-output '.VPC_resources.value[].default_vpc[].public_ip' 2>/dev/null
	cat nova.output.json | jq --raw-output '.VPC_resources.value[].custom_vpc[].public_ip' 2>/dev/null
) | grep --color=never "[1-9]"
