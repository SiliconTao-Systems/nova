#!/bin/bash

set -e
WRKDIR=$PWD
cd /tmp/
[ -d easy-rsa ] && {
	rm -rf easy-rsa
}
git clone https://github.com/OpenVPN/easy-rsa.git

cd easy-rsa/easyrsa3
./easyrsa init-pki

EASYRSA_REQ_COUNTRY=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.country')
EASYRSA_REQ_PROVINCE=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.province')
EASYRSA_REQ_CITY=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.location')
EASYRSA_REQ_EMAIL=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.email')
EASYRSA_REQ_ORG=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.organization')
EASYRSA_REQ_OU=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.unit')
DOMAIN=$(cat ${WRKDIR}/nova.json|jq --raw-output '.site.domain')

echo '
set_var EASYRSA_REQ_COUNTRY    "$EASYRSA_REQ_COUNTRY"
set_var EASYRSA_REQ_PROVINCE   "$EASYRSA_REQ_PROVINCE"
set_var EASYRSA_REQ_CITY       "$EASYRSA_REQ_CITY"
set_var EASYRSA_REQ_ORG        "$EASYRSA_REQ_ORG"
set_var EASYRSA_REQ_EMAIL      "$EASYRSA_REQ_EMAIL"
set_var EASYRSA_REQ_OU         "$EASYRSA_REQ_OU"
' >> pki/vars

export EASYRSA_BATCH=1
echo "./easyrsa build-ca nopass"
./easyrsa build-ca nopass
echo "./easyrsa build-server-full server nopass"
./easyrsa build-server-full server nopass
echo "./easyrsa build-client-full ${DOMAIN} nopass"
./easyrsa build-client-full ${DOMAIN} nopass

mkdir -v $WRKDIR/key_cert
cp -v pki/ca.crt $WRKDIR/key_cert/
cp -v pki/issued/server.crt $WRKDIR/key_cert/
cp -v pki/private/server.key $WRKDIR/key_cert/
cp -v pki/issued/${DOMAIN}.crt $WRKDIR/key_cert/
cp -v pki/private/${DOMAIN}.key $WRKDIR/key_cert/
cd $WRKDIR/key_cert/

aws acm import-certificate --certificate \
	fileb://server.crt --private-key fileb://server.key \
	--certificate-chain fileb://ca.crt | jq '{ "cert": .CertificateArn}' > $WRKDIR/tmpcert.json
aws acm import-certificate --certificate \
	fileb://${DOMAIN}.crt --private-key fileb://${DOMAIN}.key \
	--certificate-chain fileb://ca.crt | jq '{ "key": .CertificateArn}' >> $WRKDIR/tmpcert.json

cd $WRKDIR
cat tmpcert.json | jq -s add | jq '{ "certs": { "cert":.cert, "key": .key } }' > key_cert.json
rm -f tmpcert.json
rm -rf /tmp/easy-rsa
