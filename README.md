# Nova: Build AWS with Terraform
# Nova
> Nova: Deep in the core of a star, intense gravity crushes atoms together and creates heavy elements. One such element is the energy-dense Uranium. All Uranium was created this way. After billions of years of burning off its energy on the surface, the star explodes as a supernova, spreading its heavy elements across the galaxy. The nova of a dead star brings forth a new creation.


Nova began as a simple demonstration of how to use [Terraform](https://www.terraform.io/) to build an Amazon Web Services (AWS) environment with a virtual machine in the  AWS Cloud known as an ``instance``. Nova provides an intuitive approach to letting the developer add apps that can talk to each other in a cloud environment which enables users to make their data unique and customizable. Think of Nova as a framework for Terraform.

Nova still can be used as an entry-level introduction to Terraform, and it now also can be configured to add more features using a Javascript Object Notation (JSON) file.

If you prefer not to use the JSON features, you can check out the original version using these commands.

```bash
git clone https://gitlab.com/SiliconTao-Systems/nova.git --branch=1.0.0
cd nova
make apply
```

Review the README.md file that comes with the older version 1.0.0 as this new version will document features that don't apply.

This video is version 1.0.0 in action.

[![Watch Nova in action](https://img.youtube.com/vi/Fcg_5s3LBT8/0.jpg)](https://www.youtube.com/watch?v=Fcg_5s3LBT8)

# Welcome to Nova 2.0
## Why Nova?
The majority of environments this project's developer has created all have a common set of AWS resources. Nova 2.0 allows anyone with basic Linux skills and knowledge of a Command Line Interface (CLI) to [stand up](## "Create and start a cloud service") an environment using those common resources by adding a description of that resource to a JSON file that contains the unique information about the environment.


The goal of Nova is to allow any project hosted on a Git repository to include a **nova.json** file that will define how to construct a hosting environment in an AWS Cloud. To demonstrate how your project could include a crafted **nova.json** file, several example files have been included. This documentation will provide direction to creating the file and examples of how to make use of these features.

For development, an editor is required. The open source Integrated Development Environment (IDE) ``codium`` is recommended.

## How To Use This
A ``Makefile`` is provided to simplify most of the steps. After installing the needed software and creating an AWS Account with IAM, the project can be set up by running ``make apply``

Using just the command ``make apply`` Nova will [stand up](## "Create and start a cloud service") the default example [mStream](# mstream).

To deploy another project hosted on a Git repository use ``make apply URL=https://some.git.site/some-project.git``. This works for public-hosted repositories and private repositories that are accessible from the administrators' system.


When done with the environment it can be destroyed to save from being charged AWS fees by running ``make destroyall``

## Software Needed
In addition to standard Linux programs such as an SSH client and shell, these programs may need extra steps to install using a package manager like ``yum`` or ``apt``.
- git
- make
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [jq](https://stedolan.github.io/jq/)
- [terraform](https://www.terraform.io/downloads.html)


# Using Nova
1. [Create An AWS Account](#create-an-aws-account) an account is needed
2. [Access Keys & Region](#access-keys--region) Terraform uses these keys to create the resources
3. [Installing Nova](#installing-nova) download this project
4. [The Nova File](#the-nova-file) start of a standard Nova file
5. [Nova Content](#nova-content) add more resources to improve the environment
6. [Building Example Projects](#building-example-projects) get started by deploying a ready-made project
    * [mStream](#mstream)
    * [Wordpress](#wordpress)
    * [MediaHouse](#media-house)

## Create An AWS Account
Open an account with [AWS](https://portal.aws.amazon.com/billing/signup). This will require a credit card. AWS offers a 12-month free tier that some resources are eligible for. Version 1.0.0 of Nova only used free tier eligible resources, Nova version 2 is able to use non-free tier resources. Read and become familiar with [AWS Free Tier](https://aws.amazon.com/free/).

The main account that is created using a credit card is the root account or account owner. The owner account can be used to create AWS resources, but it is highly recommended that if there is more than one developer or team member, each member should use an Identity and Access Management (IAM) account, and the owner account should be denied access to change resources. The owner account is typically used to manage the IAMs and access things developers are not allowed to access, such as billing information.

Create an [IAM account](https://console.aws.amazon.com/iam/home).

## Access Keys & Region
Once an account has been created for development, it requires AWS Access ID & AWS Access Key to be added to the local system that is used to deploy the AWS resources.
[Managing access keys for IAM users](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html)

Nag me to provide a simple script to do this. 😉

Next choose a region, this is the AWS data center that most of the resources will exist in. Note that some resources, like S3, are global. It is recommended to choose a region that the account is not currently using for other projects to avoid name and service collision. Here is a guide on how to choose a region [Save yourself a lot of pain (and money) by choosing your AWS Region wisely](https://www.concurrencylabs.com/blog/choose-your-aws-region-wisely/)

Once the AWS Access Key has been generated and a region is selected, use the AWS CLI to add it to the local credentials file. It is possible to manually edit the files in ``~/.aws/config`` and ``~/.aws/credentials`` but small mistakes with spacing or formatting can cause it to fail.

Instructions to add the credentials are on [Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html). Use the profile name ``default``. For this project, there can only be one profile in the credentials. If pre-existing credentials exist, rename the ``~/.aws/`` folder until done.

Test the credentials using AWS CLI.
```bash
aws iam get-user | jq
```

If successful the output will be JSON that looks much like this.
```json
{
  "User": {
    "Path": "/",
    "UserName": "TheIamUser",
    "UserId": "SOMESTRING",
    "Arn": "arn:aws:iam::<OWNER ID>:user/TheIamUser",
    "CreateDate": "YYYY-MM-DDTHH:mm:ss+00:00",
    "PasswordLastUsed": "YYYY-MM-DDTHH:mm:ss+00:00"
  }
}
```
# Installing Nova
Download the git repository of Nova.

```bash
git clone https://gitlab.com/SiliconTao-Systems/nova.git
cd nova
```
Along with the packages listed above in [Software Needed](#Software-Needed)

# The Nova File
In Nova 2, resources are configured in a file called ``nova.json``.

> _**Note**_ Where is the **nova.json** file? The **nova.json** needs to be crafted to meet the needs of each project. Select the various AWS resources that the project needs and include them in a new custom **nova.json** file. Example files are included.

All **nova.json** files must include a **site** entry.
```json
"site": {
      "domain":         "silicontao.com",
      "organization":   "Silicon Tao",
      "country":        "CA",
      "province":       "Saskatchewan",
      "location":       "Regina",
      "email":          "osgnuru@gmail.com",
      "unit":           "DevOps",
      "region":         "ca-central-1",
      "block_public":   true
   }
```

These values are common to TLS certificates with the exception of **region** and **block_public**
- **region** is the AWS region the resources will be built in
- **block_public** creates a Security Group rule to prevent public access while the website is being configured.

> _**Note**_: A Makefile recipe **unblock** is provided to open public access to web traffic. Use this if **block_public** is true.

By adding additional resources as **Nova Content** to the JSON file, Nova will build the environment with those resources.

## Nova Content
Directories for resources contain the Terraform files to set up those resources.
* [default_vpc](./vpc/README.md) has no configuration options and is only used when a custom VPC is not set
* [instance](./instance/README.md) configurations for [EC2 Instances](https://aws.amazon.com/what-is/cloud-instances/)
* [rds](./rds/README.md) configurations for Database services
* [route53](./route53/README.md) DNS resolvers for the hostnames listed in instances
* [s3](./s3/README.md) Amazon Simple Storage Service or [S3](https://aws.amazon.com/s3/) provides large, low-cost data storage that can be mounted using [s3fuse](https://github.com/s3fs-fuse/s3fs-fuse)
* [security](./security/README.md) configurations for the rules that control communication between AWS resources
* [vpc](./vpc/README.md) custom VPC with additional resources.

## Other Items In The Nova Directory
The following are not AWS resources but are documented here for clarity.
To build a project with Nova, the [Makefile](./Makefile) will be used with either a provided example **JSON** or another file in an external project.

* [setup](./setup/README.md) scripts to setup the instances
* [toolbox](./toolbox/README.md) scripts ran locally by the [Makefile](./Makefile) to assist with building the environments
* [LICENSE](./LICENSE) legal mumbo jumbo
* [main.tf](./main.tf) this is were Terraform starts doing it's thing
* [Makefile](./Makefile) that Makefile again, we'll get to that is a bit
* [mediahouse.json](./mediahouse.json) example to setup two instances in a custom VPC with an S3 mount
* [mstream.json](./mstream.json) example of a single EC2 inside the Default VPC, just like Nova 1.0.0 did
* [wordpress.json](./wordpress.json) example of a single EC2 inside the Default VPC with an RDS to host your very own [Wordpress](https://wordpress.com/)

# Building Example Projects
## mStream
Based on the Nova 1 deployment of mStream. This [mStream](./mstream.json) example creates a single instance inside a **Default_VPC**. The instance is in a public subnet and has a public IP address with direct Internet access. This example hosts the mStream service.

## Wordpress
The [wordpress.json](./wordpress.json) example also creates a single instance inside a **Default_VPC**. This example makes use of the **block_public** setting so that the administrator has time to set the admin login password before the site is exposed to the public. Use the command ``make unblock`` to remove the block after the site is configured and ready for public access.

## Media House
This example project [mediahouse.json](./mediahouse.json) builds the **mStream** instance and a **fileShare** instance inside a custom [vpc](./vpc/README.md) with attached [s3](./s3/README.md).

This deployment happens in two steps, the first creates the VPC with a VPN, and the second builds the instances because they require the VPN to be active before they can connect via SSH.

# Other Useful Tips

## Image Selection
Using AWS CLI and JQ to get information about an image.
```bash
aws ec2 describe-images --owners self amazon --filters "Name=root-device-type,Values=ebs" | jq '.[]|.[]|select(.ImageId == "ami-046a5648dee483245")'
```

## Links
[AWS diagrams](https://online.visual-paradigm.com/w/wteopwgz/drive/#infoart:proj=0&documents)
[Getting started with Terraform](https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started)
[Alpine Linux](https://alpinelinux.org/) tiny capable Linux
[mStream](https://www.mstream.io/) music streaming service
[WordPress Themes](https://en-ca.wordpress.org/themes/) worlds most popular content service
[Free package hosting for open source projects](https://cloudsmith.io/~silicontao/repos/)

