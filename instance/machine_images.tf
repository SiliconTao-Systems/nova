# ami-0a31e7dfd86d40bad
# alpine-3.15.6-x86_64-bios-cloudinit-r0

# ami-046a5648dee483245
# amzn2-ami-kernel-5.10-hvm-2.0.20220912.1-x86_64-gp2

variable "machine_image" {
	default = {
		"alpine_linux" = {
			ami 			=	"ami-0a31e7dfd86d40bad"
			username	= "alpine"
		},
		"amazon_linux_2" = {
			ami 			=	"ami-046a5648dee483245"
			username	= "ec2-user"
		}
	}
}
