variable "AvailabilityZones" {
	description	= "Availability Zones for the selected region."
	default			= []
}

variable "default_vpc" {
	description = "The default VPC used if a VPC is not created."
	default 		= []
}

variable "home_dir" {
	description = "Users home"
}

variable "instances" {
	description = "The nova.json instances"
}

variable "nova_file" {
	description = "The NOVA.json file"
}

# variable "payload_dir" {
# 	type = string
# }

variable "rds_endpoints" {
	description = "Database endpoints"
	default			= []
}

variable "roles" {
	type = map
}

variable "s3" {
	description = "Nova.json values for S3"
	default 		= []
}

variable "sgs" {
	type = map
}

variable "site" {
	description = "Nova.json values for the site"
}

variable "ssh_keys" {
	description = "Locally generated key pair."
	type = map
}

variable "vpc" {
	description	= "The nova.json vpc"
	default			= []
}

variable "vpc_resources" {
	description = "AWS VPC resources"
	default			= []
}
