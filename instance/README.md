By adding ``instances`` to the project JSON file, an EC2 Instance will be created in AWS Cloud using the following configurations.

* **hostname** the name of the system
* **distro** the name of the image use selected from [machine_images.tf](./machine_images.tf)
* **insance_type** sets the processor, number of cores and RAM as selected from [Instance Types](https://aws.amazon.com/ec2/instance-types/)
* **volume_size** set the size of the root disk in GiB.
* **setup_scripts** scripts to be ran from [setup](../setup/)
* **security_groups** the network communication rules that this system will be able to use

```JSON
	"instances": [
		{
			"hostname": "mstream",
			"distro":	"alpine_linux",
			"instance_type": "t2.micro",
			"volume_size": 5,
			"setup_scripts": [
				"s3",
				"mstream"
			],
			"security_groups": [
				"ec2-server",
				"ssh-server",
				"web-server"
			]
		}
	]
```

Instances is an array, and as such, more instances can be defined by adding on to the array.
```JSON
	"instances": [
		{
			"hostname": "server1",
			"distro":	"alpine_linux",
			"instance_type": "t2.micro",
			"volume_size": 5,
			"security_groups": [
				"ec2-server",
				"ssh-server",
			]
		},
		{
			"hostname": "server2",
			"distro":	"alpine_linux",
			"instance_type": "t2.micro",
			"volume_size": 5,
			"security_groups": [
				"ec2-server",
				"ssh-server",
			]
		},
		{
			"hostname": "server3",
			"distro":	"alpine_linux",
			"instance_type": "t2.micro",
			"volume_size": 5,
			"security_groups": [
				"ec2-server",
				"ssh-server",
			]
		}
	]
```

