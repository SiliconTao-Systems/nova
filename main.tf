provider "aws" {
	region		= var.site.region
	access_key	= var.access_key
	secret_key	= var.secret_key
}

# module "iam" {
# 	source = "./iam"
# }

module "default_vpc" {
	source						= "./default_vpc"
	#AvailabilityZones	= var.AvailabilityZones
	count 						= length(var.vpc) > 0 ? 0 : 1
	site							= var.site
}

module "instance" {
	source        		= "./instance"
	AvailabilityZones	= var.AvailabilityZones
	count 						= length(var.instances) > 0 && var.twostage == 0 ? 1 : 0
	default_vpc				= length(var.vpc) > 0 ? null : module.default_vpc[0].default_vpc
	home_dir					= var.home_dir
	instances					= var.instances
	nova_file					= var.nova_file
	rds_endpoints 		= length(var.rds) > 0 ? module.rds[0].rds_endpoints : []
	roles							= length(var.s3) > 0 ? module.s3[0].roles : null
	s3  							= length(var.s3) > 0 ? var.s3 : []
	sgs								= module.security.sgs
	site							= var.site
	ssh_keys					= var.ssh_keys
	vpc								= var.vpc
	vpc_resources			= length(var.vpc) > 0 ? module.vpc[0].vpc_resources : null
}

module "rds" {
	source						= "./rds"
	AvailabilityZones	= var.AvailabilityZones
	count 						= length(var.rds) > 0 ? 1 : 0
	default_vpc				= length(var.vpc) > 0 ? null : module.default_vpc[0].default_vpc
	rds								= var.rds
	site							= var.site
	sgs								= module.security.sgs
	vpc								= var.vpc
	vpc_resources			= length(var.vpc) > 0 ? module.vpc[0].vpc_resources : null
}

module "route53" {
	source			= "./route53"
	count 			= length(var.instances) > 0 && var.twostage == 0 ? 1 : 0
	host_ips		= module.instance[0].host_ips
	site				= var.site
}

module "s3" {
	source 				= "./s3"
	#bucket_name 	= length(var.s3) > 0 ? module.s3[0].bucket_name : []
	count 				= length(var.s3) > 0 ? 1 : 0
}

module "security" {
	source 				= "./security"
	default_vpc		= length(var.vpc) > 0 ? null : module.default_vpc[0].default_vpc
	site					= var.site
	vpc						= length(var.s3) > 0 ? var.vpc : []
	vpc_resources	= length(var.vpc) > 0 ? module.vpc[0].vpc_resources : null
}

module "vpc" {
	source						= "./vpc"
	AvailabilityZones	= var.AvailabilityZones
	count 						= length(var.vpc) > 0 ? 1 : 0
	certs							= var.certs
	instances					= var.instances
	aws_instances			= length(module.instance) > 0 ? module.instance[0].aws_instances : []
	sgs								= module.security.sgs
	site							= var.site
	vpc								= var.vpc
}

# output "public_ip" {
# 	value = [ aws_instance.linux["${count.index}"].public_ip ]
# }

output "VPC_resources" {

	# This does not work because the two types are different
	# value 	= length(module.vpc) > 0 ? toset(module.vpc[0].vpc_resources) : module.instance[0].host_ips
	#value 	= length(var.vpc) > 0 ? toset(module.vpc[0].vpc_resources) : toset(module.instance[0].host_ips)

	# This only works for custom vpc
	#value 	= length(module.vpc) > 0 ? module.vpc[0].vpc_resources : null

	# This only works for default_vpc
	#value 	= length(module.vpc) > 0 ? [] : module.instance[0].host_ips

	value = [{
		"custom_vpc":		length(module.vpc) > 0 ? module.vpc[0].vpc_resources : null
		"default_vpc":	length(module.vpc) > 0 ? [] : module.instance[0].host_ips
}]
}

output "vpn_endpoint" {
	value 	= length(module.vpc) > 0 ? module.vpc[0].vpn_endpoint : []
}

output "instances" {
	value = length(var.instances) > 0 ? module.instance : []
}

output "elb" {
	value = module.vpc[0].elb
}