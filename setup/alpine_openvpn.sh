#!/bin/ash

# This still needs work.
# The EC2 VPN may not work for building with Nova because the EC2s requrie
# SSH connection to setup the software, the VPN would need to be finished setup
# before any other EC2 instances could begin. Terraform needs to use
# -parallelism=1 to only build one at a time an the VPN needs to be first.
# https://wiki.alpinelinux.org/wiki/Setting_up_a_OpenVPN_server

apk update
apk add openvpn
apk add openssl
apk add easy-rsa
rc-update add openvpn default
modprobe tun
echo "tun" >> /etc/modules-load.d/tun.conf
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.d/ipv4.conf
sysctl -p /etc/sysctl.d/ipv4.conf

set -e
KEY_PATH="/etc/openvpn/keys"
mkdir -pv ${KEY_PATH}/easy-rsa
echo "Generating self-signed certificates..."
# Generate a key and cert siging request
COUNTRY=$(cat nova.json | jq --raw-output '.site.country')
STATE=$(cat nova.json | jq --raw-output '.site.state')
LOCATION=$(cat nova.json | jq --raw-output '.site.location')
DOMAIN=$(cat nova.json | jq --raw-output '.site.domain')
openssl genrsa -out ${KEY_PATH}/key.pem
openssl req -new -key ${KEY_PATH}/key.pem -out ${KEY_PATH}/csr.pem -subj "/C=${COUNTRY}/ST=${STATE}/L=${LOCATION}/CN=${DOMAIN}"

# Sign the request with our own key to create a self-signed cert
openssl x509 -req -days 9999 -in ${KEY_PATH}/csr.pem -signkey ${KEY_PATH}/key.pem -out ${KEY_PATH}/cert.pem

#openssl pkcs12 -in PFXFILE -cacerts -nokeys -out ca.pem
#openssl pkcs12 -in PFXFILE -nokeys -clcerts -out cert.pem
#openssl pkcs12 -in PFXFILE -nocerts -nodes -out key.pem

cat >  /etc/openvpn/openvpn.conf <<@EOF
local "Public Ip address"
port 1194
proto udp
dev tun
ca /etc/openvpn/easy-rsa/keys/ca.crt
cert ${KEY_PATH}/cert.pem
key ${KEY_PATH}/key.pem
# dh /etc/openvpn/easy-rsa/keys/dh1024.pem # If you changed to 2048, change that here!
server 10.0.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "route 10.0.0.0 255.0.0.0"
push "dhcp-option DNS 10.0.0.1"
keepalive 10 120
comp-lzo
user nobody
group nobody
persist-key
persist-tun
status /var/log/openvpn-status.log
log-append  /var/log/openvpn.log
verb 3
@EOF

openvpn --config /etc/openvpn/openvpn.conf


cat > ~alpine/openvpn.ovpn <<@EOF
client
dev tun
proto udp
remote "public IP" 1194
resolv-retry infinite
nobind
ns-cert-type server # This means the certificate on the openvpn server needs to have this field. Prevents MitM attacks
persist-key
persist-tun
ca client-ca.pem
cert client-cert.pem
key client-key.pem
comp-lzo
verb 3
@EOF

