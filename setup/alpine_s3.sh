#!/bin/ash

set -e

AWS_ACCOUNT_ID=$(cat environment.json | jq --raw-output '.owner[].id')
AWS_REGION=$(cat nova.json | jq --raw-output '.site.region')
# Requires /etc/mime.types in mailcap
apk add mailcap
S3_BUCKET_NAME="s3storage-${AWS_ACCOUNT_ID}"
# apk add s3fs-fuse
echo "Build s3fs-fuse from source"
apk --update --no-cache add fuse alpine-sdk automake autoconf libxml2-dev fuse-dev curl-dev git bash
# git clone https://github.com/s3fs-fuse/s3fs-fuse.git
git clone --branch v1.91 --single-branch https://github.com/s3fs-fuse/s3fs-fuse.git
cd s3fs-fuse
./autogen.sh
./configure --prefix=/usr
make
make install
make clean
echo "Build s3fs-fuse from source"
mkdir -pv /s3

#modprobe fuse
#s3fs -o iam_role='S3_Access' ${S3_BUCKET_NAME} /s3/
# aws s3 cp *.json s3://${S3_BUCKET_NAME}
# aws s3 ls s3://${S3_BUCKET_NAME}
#echo "s3fs#${S3_BUCKET_NAME} /s3 fuse rw,iam_role=S3_Access 0 0" >> /etc/fstab

echo "s3:2345:boot:/usr/local/bin/mount.s3fs" >> /etc/inittab
S3_BUCKET_NAME="s3storage-${AWS_ACCOUNT_ID}"

cat > /usr/local/bin/mount.s3fs <<@EOF
#!/bin/ash

modprobe fuse
s3fs -o iam_role='S3_Access' -o allow_other ${S3_BUCKET_NAME}:/${AWS_REGION}/ /s3/
@EOF

chmod 755 /usr/local/bin/mount.s3fs

/usr/local/bin/mount.s3fs
df -h



