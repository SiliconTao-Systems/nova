#!/bin/ash

set -e
echo "This is wordpress setup"

getent passwd| grep -q ^nginx || adduser -D -h /var/www/ nginx||:
apk add nginx

# https://wiki.alpinelinux.org/wiki/Nginx_with_PHP
# https://wordpress.org/support/article/how-to-install-wordpress/
# https://www.cyberciti.biz/faq/how-to-install-nginx-web-server-on-alpine-linux/

cat nova.json | jq '.instances | .[]|select(.hostname == "${HOSTNAME}")'
wget https://en-ca.wordpress.org/latest-en_CA.tar.gz -O latest-en_CA.tar.gz
tar xfz latest-en_CA.tar.gz -C /var/www/
chown -R nginx:nginx /var/www/
rm -v latest-en_CA.tar.gz
install -d -o nginx -g nginx /var/www/wordpress/wp-content/uploads


PHP_PACKS="fpm mcrypt soap openssl gmp pdo_odbc \
json dom pdo zip mysqli sqlite3 apcu pdo_pgsql \
bcmath gd odbc pdo_mysql pdo_sqlite gettext xmlreader \
xmlrpc bz2 iconv pdo_dblib curl ctype"
for i in $PHP_PACKS; do
	apk list --available | grep php8-${i} && apk add php8-${i} ||:
done

echo "Hostname ${HOSTNAME}"
DB_NAME=$(cat nova.json |jq --raw-output '.instances|.[]|select(.hostname == "'${HOSTNAME}'").db_name')
echo "DB name  ${DB_NAME}"
ENDPOINT=$(cat environment.json |jq --raw-output '.rds|.[][]|select(.name == "'${DB_NAME}'").endpoint'|sed -e 's/:3306$//')
echo "Endpoint = '${ENDPOINT}'"
DOMAIN=$(cat nova.json |jq --raw-output '.site.domain')
DB_USER=$(cat nova.json |jq --raw-output '.rds|.[].db_username')
DB_PASS=$(cat nova.json |jq --raw-output '.rds|.[].db_password')

cat > /etc/nginx/http.d/default.conf <<@EOF
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	server_name ${HOSTNAME}.${DOMAIN};

	# document root #
	root        /var/www/wordpress;

	# log files
	access_log  /var/log/nginx/access.log;
	error_log   /var/log/nginx/error.log;

	# cache files on browser level #
	# Directives to send expires headers and turn off 404 error logging. #
	location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|rss|atom|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
		access_log off; log_not_found off; expires max;
	}

	location ~ \.php$ {
		fastcgi_pass		127.0.0.1:9000;
		fastcgi_index		index.php;
		include					fastcgi.conf;
	}

	location / {
		index						index.php;
		try_files       \$uri    \$uri/index.php?\$args;
	}

}
@EOF

CONF=/var/www/wordpress/wp-config.php
cp /var/www/wordpress/wp-config-sample.php $CONF
sed -i ${CONF} -e '/^define.*DB_NAME/{s/database_name_here/'${DB_NAME}'/}'
sed -i ${CONF} -e '/^define.*DB_USER/{s/username_here/'${DB_USER}'/}'
sed -i ${CONF} -e '/^define.*DB_PASSWORD/{s/password_here/'${DB_PASS}'/}'
sed -i ${CONF} -e '/^define.*DB_HOST/{s/localhost/'${ENDPOINT}'/}'

sed -i /etc/php8/php.ini -e 's/^upload_max_filesize = .*/upload_max_filesize = 10M/'
sed -i /etc/nginx/nginx.conf -e 's/client_max_body_size = .*/client_max_body_size = 10m/'
echo "user = nginx" >> /etc/php8/php-fpm.conf

rc-update add nginx default
rc-update add php-fpm8 default

rc-service nginx start
rc-service php-fpm8 start

exit 0


