#!/bin/ash

set -e
# https://wiki.alpinelinux.org/wiki/Setting_up_a_samba-server
apk add samba

SHARE="/media/storage"
mount | grep '/s3' && SHARE="/s3"
mkdir -pv ${SHARE}
chmod 0777 ${SHARE} ||:

cat > /etc/samba/smb.conf <<-!END
[global]
   workgroup = AWS_GROUP
   min protocol = SMB2
   protocol = SMB3
   disable netbios = yes
   hosts allow = 127. 10. 172.
   server string = Samba Server
   server role = standalone server
   max log size = 50
   dns proxy = no

[storage]
   comment = Storage
   # to follow symlinks
   follow symlinks = yes
   # to allow symlinks from outside
   wide links = yes
   guest ok = yes
   public = yes
   force user = root
   browseable = yes
   writeable = yes
   create mask = 0644
   directory mask = 0755
   path = ${SHARE}

!END

# adduser alpine
rc-update add samba
rc-service samba start
smbstatus
