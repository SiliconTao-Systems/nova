#!/bin/ash

set -e

cat nova.json | jq '.instances | .[]|select(.hostname == "${HOSTNAME}")'

# The S3 is to slow for mStream and causes ELB to timeout and disconnect from the frontend
#ln -s /s3 /home/alpine/Music
mkdir /home/alpine/Music

apk add --update npm nodejs git jq

# Instructions taken from https://github.com/IrosTheBeggar/mStream

git clone https://github.com/IrosTheBeggar/mStream.git
cd mStream

npm install
npm link

npm install pm2 -g

JF=save/conf/default.json
[ -f ${JF} ] || echo "{}" > ${JF}
jq -a '.+{ "port": "80", "folders": { "music": { "root": "/home/alpine/Music/" } } }' ${JF} > ${JF}.swap
mv ${JF}.swap ${JF}
cat ${JF} | jq

cat > /etc/init.d/mStream <<EOF
#!/sbin/openrc-run

pidfile=/var/run/node.pid
procname=/usr/bin/node

start() {
        cd /home/alpine/setup/mStream
        ebegin "Starting \${SVCNAME}"
        start-stop-daemon --start --pidfile \${pidfile} --exec node --background cli-boot-wrapper.js
        eend \$?
}

stop() {
        ebegin "Stopping \${SVCNAME}"
        killall node
        eend \$?
}

restart() {
	stop
	wait;
	start
}

status() {
        netstat -naptu | grep -q ":80.*LISTEN.*node" && {
                echo "Running"
        } || {
                echo "Stopped"
                exit 1
        }

}

EOF
chmod a+x /etc/init.d/mStream
rc-update add mStream
rc-service mStream start

# this did not work
# nohup node cli-boot-wrapper.js &

