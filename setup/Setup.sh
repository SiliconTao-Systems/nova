#!/bin/sh

set -e
export HNS=$1
export HOSTNAME=$1
OS_NAME=$(grep ^"ID=" /etc/os-release | cut -d= -f2)
export SETUP_DIR=$PWD

function Color {
	which tput > /dev/null 2>&1 && {
		TputColor $1
	} || {
		EchoColor $1
	}
}

function TputColor {
	tput bold
	case $1 in
		black)  tput setaf 0;;
		red)    tput setaf 1;;
		green)  tput setaf 2;;
		yellow) tput setaf 3;;
		blue)   tput setaf 4;;
		magenta)tput setaf 5;;
		cyan)   tput setaf 6;;
		white)  tput setaf 7;;
		off)    tput sgr0;;
	esac
}

function EchoColor
{
	case $1 in
		"red")    echo -ne "\33[01;31m";;
		"green")  echo -ne "\33[01;32m";;
		"yellow") echo -ne "\33[01;33m";;
		"blue")   echo -ne "\33[01;34m";;
		"purple") echo -ne "\33[01;35m";;
		"cyan")   echo -ne "\33[01;36m";;
		"white")  echo -ne "\33[01;37m";;
		"grey")   echo -ne "\33[01;39m";;
		"off")    echo -ne "\33[0m";;
	esac
}

function Header {
	Color yellow
	# printf "%0.s-" {1..40} # This does not work in ash the Alpine shell
	# printf "\n    "
	echo "----------------------------------------"
	Color cyan
	printf "%s   \n" "$1"
	Color yellow
	# printf "%0.s-" {1..40}
	# printf "\n    "
	echo "----------------------------------------"
	Color off
}


# echo "Run Setup_${HNS}.sh"
# chmod 755 Setup_${HNS}.sh
# ls -lh ./Setup_${HNS}.sh
# ./Setup_${HNS}.sh
[ -f ${OS_NAME}.sh ] && {
	Header "${OS_NAME}.sh"
	cd $SETUP_DIR
	source ${OS_NAME}.sh
}

cd $SETUP_DIR
# cat nova.json | jq '.instances | .[]|select(.hostname == "${HOSTNAME}")'
DISTRO=$(cat nova.json|jq --raw-output '.instances | .[]|select(.hostname == "'$(hostname)'").distro')
cat nova.json|jq --raw-output '.instances | .[]|select(.hostname == "'$(hostname)'").setup_scripts|.[]' | while read LINE; do
	cd $SETUP_DIR
	[ -f "${OS_NAME}_${LINE}.sh" ] && {
		Header "${OS_NAME}_${LINE}.sh"
		source "${OS_NAME}_${LINE}.sh"
	} ||  {
		echo "Could not find '${OS_NAME}_${LINE}.sh'"
	}
done

[ -f "Setup_${HNS}.sh" ] && {
	Header "Setup_${HNS}.sh"
	cd $SETUP_DIR
	source "Setup_${HNS}.sh"
}

df -h
