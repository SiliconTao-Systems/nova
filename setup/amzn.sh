#!/bin/bash

yum -y update
amazon-linux-extras install -y epel
yum -y install awscli
yum -y install jq
yum -y install s3fs-fuse