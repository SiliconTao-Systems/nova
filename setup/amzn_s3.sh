#!/bin/bash

# The RPM works for AL2
yum -y install s3fs-fuse
AWS_ACCOUNT_ID=$(cat environment.json | jq --raw-output '.owner[].id')
AWS_REGION=$(cat nova.json | jq --raw-output '.region')
echo "s3fs#s3storage-${AWS_ACCOUNT_ID}:${AWS_REGION}/ /s3 fuse rw,iam_role=S3_Access 0 0" >> /etc/fstab
mount -a
df -h

# If you want to build the source, this is how it was tested against the Alpine version.
# yum group install "Development Tools"
# yum install fuse-devel
# yum install libcurl-devel
# yum install libxml2-devel
# yum install openssl-devel
# git clone --branch v1.91 --single-branch https://github.com/s3fs-fuse/s3fs-fuse.git
# cd s3fs-fuse
# ./configure
# make
# make install