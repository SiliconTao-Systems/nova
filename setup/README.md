# Setup
These scripts are used by the instances to setup features.

[Setup.sh](./Setup.sh) is common to all instances and it will execute distro scripts, then service scripts, followed by host scripts.

## Distro Scripts
The format of these scripts are ``<distro>.sh``

These are pre setup scripts common to all services and hosts but unique for each distribution. They perform package updates and install basic packages needed by Nova.
* [alpine.sh](https://www.alpinelinux.org/) a lite weight Linux using [BusyBox](https://busybox.net/)
* [amzn.sh](https://aws.amazon.com/amazon-linux-2) Amazon Linux 2 based on [Red Hat Enterprise Linux](https://www.redhat.com/)

## Service Scripts
The format of these files are ``<disto name>_<service name>.sh``

* [alpine_nginx.sh](./alpine_nginx.sh) setup a basic [nginx](https://nginx.org/en/) configuration for the instance running a web service
* [alpine_s3.sh](./alpine_s3.sh) compiles [s3fuse](https://github.com/s3fs-fuse/s3fs-fuse) and mounts the S3 if it is included in the **nova.json**
* [alpine_samba.sh](./alpine_samba.sh) sets up an open [Samba](https://www.samba.org/) that can be accessed on the subnet. Do not use with [default_vpc](../vpc/README.md) as it will grant public access from the Internet.
* [amzn_s3.sh](./amzn_s3.sh) installs the [s3fs-fuse](https://github.com/s3fs-fuse/s3fs-fuse) RPM package and mounts the S3 if it is included in the **nova.json**

## Host Scripts
The format of these scripts are ``Setup_<hostname>.sh``.

These are the unique features for the host and the services it will host.

* [Setup_fileShare.sh](./Setup_fileShare.sh) uses [alpine_s3.sh](./alpine_s3.sh) and [alpine_samba.sh](./alpine_samba.sh) to host a very large file share.
* [Setup_mstream.sh](./Setup_mstream.sh) downloads git source and sets up the [mStream](./mstream.json) media service
* [Setup_wordpress.sh](./Setup_wordpress.sh) uses [rds](../rds/README.md) and creates a [Wordpress](https://wordpress.com/) service