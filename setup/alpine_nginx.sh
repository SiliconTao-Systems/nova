#!/bin/ash

set -e

AWS_ACCOUNT_ID=$(cat environment.json | jq --raw-output '.owner[].id')
AWS_REGION=$(cat nova.json | jq --raw-output '.site.region')
# Requires /etc/mime.types in mailcap
apk add nginx




