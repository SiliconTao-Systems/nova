#!/bin/ash

echo 'alias ll="ls -lh"' >> /etc/profile.d/alias.sh

hostname ${HNS}
echo ${HNS} > /etc/hostname

apk update
apk add jq
apk add aws-cli
apk add ncurses
apk add vim
#apk add net-tools