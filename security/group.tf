data "http" "myip" {
	url = "http://ipv4.icanhazip.com"
}

resource "aws_security_group" "ec2-server" {
	name				= "ec2-server"
	description	= "Egress resolver"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	egress {
		from_port					= 53
		to_port						= 53
		protocol					= "udp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
		description				= "Egress resolver"
	}
	egress {
		from_port					= -1
		to_port						= -1
		protocol					= "icmp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
		description				= "Egress ICMP"
	}
	ingress {
		from_port					= -1
		to_port						= -1
		protocol					= "icmp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
		description				= "Ingress ICMP"
	}
	egress {
		from_port 				= "80"
		to_port						= "80"
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
		description				= "Egress HTTP"
	}
	egress {
		from_port 				= "443"
		to_port						= "443"
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
		description				= "Egress HTTPS"
	}
}

resource "aws_security_group" "smb-server" {
	name        = "smb-server"
	description = "Ingress Samba"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	ingress {
		from_port					= 137
		to_port						= 139
		protocol					= "udp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	ingress {
		from_port					= 139
		to_port						= 139
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	ingress {
		from_port					= 445
		to_port						= 445
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	egress {
		from_port					= 137
		to_port						= 139
		protocol					= "udp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	egress {
		from_port					= 139
		to_port						= 139
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	egress {
		from_port					= 445
		to_port						= 445
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	tags = {
		Name = "SMB Server"
	}
}

resource "aws_security_group" "sql-client" {
	name        = "sql-client"
	description = "Egress SQL"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	egress {
		from_port					= 3306
		to_port						= 3306
		protocol					= "tcp"
		cidr_blocks				= length(var.vpc) > 0 ? [
			var.vpc_resources.vpc_prod.cidr_block
			] :	[
				"172.0.0.0/8"
				# var.default_vpc[0].cidr_block,
				# var.default_vpc[1].cidr_block
			]
		ipv6_cidr_blocks 	= [ ]
	}
	tags = {
		Name = "SQL Client"
	}
}

resource "aws_security_group" "sql-server" {
	name        = "sql-server"
	description = "Ingress SQL"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	ingress {
		from_port					= 3306
		to_port						= 3306
		protocol					= "tcp"
		cidr_blocks				= length(var.vpc) > 0 ? [
			var.vpc_resources.vpc_prod.cidr_block
			] :	[
				"172.0.0.0/8"
				# var.default_vpc[0].cidr_block,
				# var.default_vpc[1].cidr_block
			]
		ipv6_cidr_blocks 	= [ ]
	}
	tags = {
		Name = "SQL Server"
	}
}

resource "aws_security_group" "ssh-admin" {
	name        = "ssh-admin"
	description = "Ingress SSH port 22 from Terraform builder"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	ingress {
		from_port					= 22
		to_port						= 22
		protocol					= "tcp"
		cidr_blocks				= [ "${chomp(data.http.myip.body)}/32" ]
		ipv6_cidr_blocks 	= [ ]
	}
	tags = {
		Name = "SSH Admin"
	}
}

resource "aws_security_group" "ssh-server" {
	name        = "ssh-server"
	description = "Ingress SSH port 22 from anywhere"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	ingress {
		from_port					= 22
		to_port						= 22
		protocol					= "tcp"
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
		description				= "Ingress SSH"
	}
}

resource "aws_security_group" "web-server" {
	name        = "web-server"
	description = "Ingress HTTP ports 80 & 443"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	ingress {
		from_port					= "80"
		to_port						= "80"
		protocol					= "tcp"
		cidr_blocks 			= var.site.block_public == true ? [ "${chomp(data.http.myip.body)}/32" ] : [ "0.0.0.0/0" ]
		ipv6_cidr_blocks 	= var.site.block_public == true ? [ ] : ["::/0"]
	}
	ingress {
		from_port					= "443"
		to_port						= "443"
		protocol					= "tcp"
		cidr_blocks 			= var.site.block_public == true ? [ "${chomp(data.http.myip.body)}/32" ] : [ "0.0.0.0/0" ]
		ipv6_cidr_blocks 	= var.site.block_public == true ? [ ] : ["::/0"]
	}
	tags = {
		Name = "Web Server"
	}
}

resource "aws_security_group" "vpn-server" {
	name        = "vpn-server"
	description = "Ingress & Egress all ports everywhere"
	vpc_id			= length(var.vpc) > 0 ? var.vpc_resources.vpc_prod.id : null
	egress {
		from_port					= 0
		to_port						= 0
		protocol					= -1
		cidr_blocks 			= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	ingress {
		from_port					= 0
		to_port						= 0
		protocol					= -1
		cidr_blocks				= ["0.0.0.0/0"]
		ipv6_cidr_blocks 	= ["::/0"]
	}
	tags = {
		Name = "VPN Server"
	}
}
