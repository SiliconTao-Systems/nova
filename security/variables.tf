variable "admin_cidr" {
	description	= "The public IP address of the system running Terraform"
	default = {
		"ipv4"	= "127.0.0.0/24"
		"ipv6"	= "::1/128"
	}
}

variable "default_vpc" {
	description = "The default VPC used if a VPC is not created."
	default 		= []
}

variable "site" {
	description = "Nova.json values for the site"
}

variable "vpc" {
	description = "The nova.json vpc"
	default			= []
}

variable "vpc_resources" {
	description = "AWS VPC resources"
	default			= []
}


# the public block does not work, it is still open to the public
# The makefile does not open the IP at the end, no ssh, no browser