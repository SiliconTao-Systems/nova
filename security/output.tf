output "sgs" {
	value = {
		ec2-server	= aws_security_group.ec2-server.id,
		smb-server	= aws_security_group.smb-server.id,
		sql-client	= aws_security_group.sql-client.id,
		sql-server	= aws_security_group.sql-server.id,
		ssh-admin		= aws_security_group.ssh-admin.id,
		ssh-server	= aws_security_group.ssh-server.id,
		web-server	= aws_security_group.web-server.id,
		vpn-server	= aws_security_group.vpn-server.id,
	}
}