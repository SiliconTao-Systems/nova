# AWS Security Groups
These are the rules that control network communication between AWS resources. They are analogous to firewall rules, but can be applied to any AWS resource. This is important because most AWS resources are not virtual machines, but rather service or feature that does not have a computer that can be accessed. They offer greater flexibility than a firewall rule allowing a single rule to be applied across any number of varying resources of any type, thus greatly simplifying the administration of the project.

These security groups are always created and can be applied to any [instance](../instance/README.md) or [rds](../rds/README.md)

* ec2-server common to all instances, allows outgoing DNS and web requests
* smb-server allows incoming SMB
* sql-client allows outgoing SQL
* sql-server allow incoming SQL
* ssh-admin	allows incoming SSH from the administrators IP address
* ssh-server allows incoming SSH from anywhere
* web-server allows incoming HTTP & HTTPS from anywhere
* vpn-server allows incoming VPN from anywhere, use only with custom [VPC](../vpc/README.md)