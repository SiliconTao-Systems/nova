output "default_vpc" {
	value = {
		default_subnet_a = aws_default_subnet.default_subnet_a
		default_subnet_b = aws_default_subnet.default_subnet_b
	}
}