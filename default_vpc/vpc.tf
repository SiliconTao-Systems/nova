resource "aws_default_vpc" "default" {
  enable_dns_hostnames    = true
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_subnet" "default_subnet_a" {
  availability_zone       = "${var.site.region}a"
  map_public_ip_on_launch = true
  tags              = {
    Name = "Default VPC A"
  }
}

resource "aws_default_subnet" "default_subnet_b" {
  availability_zone       = "${var.site.region}b"
  map_public_ip_on_launch = true
  tags              = {
    Name = "Default VPC B"
  }
}

